# PvNP (AKA *Plants Vs. NonPlants*) #

* A simple, text-based, tower-defense game totally not based on EA/Popcap's Plants Vs. Zombies
* Developed with Python 3.4

### What is this repository for? ###

* Development and testing of a final project for Hack High School Camp

### How do I get set up? ###

* Clone the repo
* Run the `PvNP.py` file with an included game file or one of your
* For Example:
> `cd final`  
> `python PvNP.py ../examples/example_gamefile`  
* Or:
> `python3 PvNP.py YOUR_OWN_FILE` 

### Game Files ###
* See the included example files for examples of valid game files
* Game files codify the follwing information
	- The amount of cash the player starts with
	- The height and width of the playing field
	- Any number of waves, each containing:
		- The turn number during which the wave of zombies is released
		- The row in which the zombies are to be released
		- The quantity of zombies to be released
* Game files conform to the following format:
> Cash Height Width  
> Turn Row Quantity  
> Turn Row Quantity  
> ...

### How To Play ###
* Each turn, the player will be asked for keyboard input
* They have a choice of:
	- Placing a plant (entering a **ROW** number and **COLUMN** number)
	- Drawing a powerup card (entering **C**) which temporarily increases damage by a random amount for all existing plants
	- Quitting the game (entering **Q**)
	- Doing Nothing (entering **NOTHING** / pressing **ENTER**)
* Non-plants enter from the right and move forward one position each turn
* Non-plants are shown with a number, indicating how many occupy that position, or a `#` if more than 10 occupy that position
* Plants can be placed in any empty position on the field
* Plants are shown as `P` characters
* Empty spaces are shown with `.` characters
* Plants attack the first NonPlant anywhere in front of them
* NonPlants only attack plants one position in front of them
* If all waves are eradicated, the plants win
* If a non-plant reaches the far left of the field, the non-plants win

### Testing & Automation ###
* Run the `test.sh` script and check `results.txt`
* Writing your own tests
	- Testing requires 2 things:
		1. A test file (eg. `testXX`)
		2. An automated sequence file (eg. `test_sequenceXX`)
	- Test files describe the cash, field, and waves, like usual
	- Test sequences contain a sequence of actions for the automated player to take.
	- Test sequences must contain exactly one action for each turn
	- Sequences can contain any number of these three actions:
		1. `place_plant ROW COL`
		2. `draw_card`
		3. `Nothing`
	- To run a single test, do `python test.py TESTFILE TESTSEQUENCE`
* To automate your tests, add your test files and sequences to the `tests` directory and name them appropriately. Then modify the NUM_TESTS variable in the `test.sh` script to include your test numbers.