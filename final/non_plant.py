# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    nonPlant.py                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: wto <marvin@42.fr>                         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/22 15:10:52 by wto               #+#    #+#              #
#    Updated: 2018/03/22 15:10:58 by wto              ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

class NonPlant:

	worth = 20

	def __init__(self):
		self.hp = 80
		self.dmg = 5

	def attack(self, plant):
		plant.take_damage(self.dmg)

	def take_damage(self, damage):
		self.hp -= damage

	def get_health(self):
		return self.hp
