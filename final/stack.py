# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    stack.py                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: wto <marvin@42.fr>                         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/22 15:11:44 by wto               #+#    #+#              #
#    Updated: 2018/03/22 15:11:46 by wto              ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

class Stack:
	def __init__(self):
		self.list = []

	def peek(self):
		if len(self.list) > 0:
			return self.list[len(self.list) - 1]
		return None

	def push(self, item):
		self.list.append(item);

	def pop(self):
		return self.list.pop()

	def size(self):
		return len(self.list)

	def is_empty(self):
		return self.size() == 0
