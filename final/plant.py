# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    plant.py                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: wto <marvin@42.fr>                         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/22 15:11:08 by wto               #+#    #+#              #
#    Updated: 2018/03/22 15:11:08 by wto              ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

class Plant:

	cost = 35

	def __init__(self):
		self.hp = 35
		self.dmg = 10
		self.powerup = 0

	def attack(self, zombie):
		zombie.take_damage(self.dmg + self.powerup)

	def take_damage(self, damage):
		self.hp -= damage

	def get_health(self):
		return self.hp

	def apply_powerup(self, card):
		self.powerup += card.power()

	def weaken_powerup(self):
		self.powerup /= 2
