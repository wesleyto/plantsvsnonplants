# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    game.py                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: wto <marvin@42.fr>                         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/22 15:10:10 by wto               #+#    #+#              #
#    Updated: 2018/03/22 15:10:29 by wto              ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

from linked_list import LinkedList
from queue import Queue
from stack import Stack
from wave import Wave
from non_plant import NonPlant
from plant import Plant
from card import Card
import random

class Game:
	def __init__(self, file):
		with open(file, 'r') as f:
			self.cash, self.height, self.width = [int(x) for x in f.readline().split(' ')]
			self.waves = LinkedList()
			self.waves_num = 0
			for line in iter(f.readline, ''):
				self.waves.add(Wave(*[int(x) for x in line.split(' ')]))
				self.waves_num += 1
		self.board = [[Queue() for j in range(self.width)] for i in range(self.height)]
		self.over = False;
		self.nonplants = 0;
		self.turn = 0;
		self.cards = Stack()
		for i in range(100):
			self.cards.push(Card(random.randint(0, 5)))

	def draw(self):
		print("Cash: $", self.cash, "\nWaves: ", self.waves_num, sep = '')
		s = '  '.join([str(i) for i in range(self.width - 1)])
		print('  ', s)
		for row in range(self.height):
			s = []
			for col in range(self.width):
				if self.is_plant(row, col):
					char = 'P'
				elif self.is_nonplant(row, col):
					size = self.board[row][col].size()
					char = str(size) if size < 10 else "#"
				else:
					char = '.'
				s.append(char)
			print(row, '  ', '  '.join(s), '\n', sep='')
		print()

	def is_over(self):
		return self.over

	def is_nonplant(self, row, col):
		return (type(self.get(row, col)).__name__ == 'NonPlant');

	def is_plant(self, row, col):
		return (type(self.get(row, col)).__name__ == 'Plant');

	def get(self, row, col):
		return self.board[row][col].peek()

	def remove(self, row, col):
		if (self.is_nonplant(row, col)):
			self.cash += NonPlant.worth
			self.nonplants -= 1
		self.board[row][col].dequeue();

	def place_nonplant(self, row):
		self.nonplants += 1
		self.board[row][self.width - 1].enqueue(NonPlant());

	def place_plant(self, row, col):
		if (self.cash >= Plant.cost and (not self.is_plant(row, col)) and (col != self.width - 1)):
				self.cash -= Plant.cost;
				self.board[row][col].enqueue(Plant());

	def place_wave(self):
		curr = self.waves.head
		while curr != None:
			if (curr.data.wave_num > self.turn):
				break
			if (curr.data.wave_num == self.turn):
				for i in range(curr.data.num):
					self.place_nonplant(curr.data.row);
				curr = curr.next
				self.waves.remove_beginning()
				self.waves_num -= 1

	def nonplant_turn(self):
		for row in range(self.height):
			for col in range(self.width):
				if self.is_nonplant(row, col):
					nonplant = self.get(row, col)
					if (col == 0):
						self.over = True;
						print("The NonPlants Won...")
						return
					elif (self.is_plant(row, col - 1)):
						plant = self.get(row, col - 1)
						for i in range(self.board[row][col].size()):
							nonplant.attack(plant)
						if (plant.get_health() <= 0):
							self.remove(row, col - 1)
					if (not self.is_plant(row, col - 1)):
						self.board[row][col - 1].combine(self.board[row][col])
						self.board[row][col] = Queue();

	def plant_turn(self):
		for row in range(self.height):
			for col in range(self.width):
				if self.is_plant(row, col):
					plant = self.get(row, col)
					for k in range(col, self.width):
						if (self.is_nonplant(row, k)):
							nonplant = self.get(row, k)
							plant.attack(nonplant)
							if (nonplant.get_health() <= 0):
								self.remove(row, k)
	def run_turn(self):
		self.turn += 1
		self.weaken_powerups()
		self.plant_turn()
		self.nonplant_turn()
		self.place_wave()
		if self.nonplants == 0 and self.waves.is_empty():
			self.over = True
			self.draw()
			print("The Plants Won!")

	def draw_card(self):
		if (self.cash >= Card.cost):
			card = self.cards.pop();
			self.apply_powerup(card)
			self.cash -= Card.cost

	def apply_powerup(self, card):
		for row in range(self.height):
			for col in range(self.width):
				if self.is_plant(row, col):
					self.get(row, col).apply_powerup(card)

	def weaken_powerups(self):
		for row in range(self.height):
			for col in range(self.width):
				if self.is_plant(row, col):
					self.get(row, col).weaken_powerup()

	def get_input(self):
		while True:
			ui = input("Action?\n\t[ROW COL] to place plant ($" +
						str(Plant.cost) +
						")\n\t[C] to draw a powerup card ($" +
						str(Card.cost) +
						")\n\t[Q] to quit\n\t[ENTER] to do nothing?\n")
			if (len(ui) > 0):
				if (len(ui) == 1):
					if (ui.lower() == 'c'):
						self.draw_card()
						break
					elif (ui.lower() == 'q'):
						self.over = True
						break
					else:
						print("Invalid Input \"" + ui + "\"")
				else:
					try:
						row, col = [int(x) for x in ui.split(' ')]
						self.place_plant(row, col)
						break
					except:
						print("Invalid Input \"" + ui + "\"")
			else:
				break
