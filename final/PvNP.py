# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    PvNP.py                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: wto <marvin@42.fr>                         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/22 15:11:26 by wto               #+#    #+#              #
#    Updated: 2018/03/22 15:11:30 by wto              ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import sys
from game import Game

if __name__ == "__main__":
	if len(sys.argv) == 2:
		game = Game(sys.argv[1]);
		game.place_wave()
		game.draw()
		game.get_input()
		while (not game.is_over()):
			game.run_turn()
			if (not game.is_over()):
				game.draw()
				game.get_input()
		print("Game Over");
