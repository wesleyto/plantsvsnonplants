class Queue:
	"""
	A Queue class
	To-Do:
		- Create an init() method that takes in no values
			- Set an instance variable to store items of the queue
		- Create a method called enqueue() which takes in data and adds it to the queue
		- Create a method called dequeue() which removes and returns the item at the front of the queue
		- Create a method called peek() which returns but DOES NOT REMOVE the item at the front of the queue
		- Create a method called size() which returns the number of items in the queue
		- Create a method called is_empty() which returns True is the queue is empty, and False otherwise
		- Create a method called combine() which takes in another queue and adds its items to this queue 
	"""