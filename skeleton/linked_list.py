class LinkedList:
	"""
	A Linked List class
	To-Do:
		- Create an init() method that takes in no values
			- Set an instance variable to store a ListNode as the head of the LinkedList
		- Create a method called add() which takes in data and adds it to a node the end of the list
		- Create a method called is_empty() which returns True when the list is empty, and False otherwise
		- Create a method called remove_beginning() which removes the first item from the list
	"""

class ListNode:
	def __init__(self, data):
		self.data = data
		self.next = None
