class Stack:
	"""
	A Stack class
	To-Do:
		- Create an init() method that takes in no values
			- Set an instance variable to store items of the stack
		- Create a method called push() which takes in data and adds it to the stack
		- Create a method called pop() which removes and returns the item on top of the stack
		- Create a method called peek() which returns but DOES NOT REMOVE the item on top of the stack
		- Create a method called size() which returns the number of items in the stack
		- Create a method called is_empty() which returns True is the stack is empty, and False otherwise
	"""