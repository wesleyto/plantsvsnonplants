# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    test.sh                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: wto <marvin@42.fr>                         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/21 18:56:21 by wto               #+#    #+#              #
#    Updated: 2018/03/21 18:56:30 by wto              ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NUM_TESTS=3
LOG=results.txt

rm $LOG &>/dev/null

for num in $(seq -f "%02g" 0 $NUM_TESTS)
do
	echo test$num >> $LOG
	python final/test.py tests/test$num tests/test_sequence$num > expected
	#change this line to run the student code
	python final/test.py tests/test$num tests/test_sequence$num > got
	DIFF=$(diff -u expected got) 
	if [ "$DIFF" != "" ]
	then
		echo $DIFF >> $LOG
	else
		echo "> OK" >> $LOG
	fi
	rm expected got
	echo >> $LOG
done